const https = require('https');
const followHttps = require('follow-redirects').https;

const { Curl } = require('node-libcurl');
const querystring = require('querystring');

var mixpanel = "f32ad24cd305efab77fd4ece4befbf7a";

module.exports = {
    async getCurrentAuthenticatedUser(req, res) {
        const requisicao = https.request({
            host: 'api-vanhack.azurewebsites.net',
            method: 'GET',
            path: '/api/services/app/LegacyUser/GetCurrentAuthenticatedUser',
            headers: {
                'Authorization': 'Bearer ' + req.headers.token
            }
        }, (resp) => {
            var str = '';

            resp.on('data', (chunk) => {
                str += chunk.toString();
            })

            resp.on('end', function () {
                return res.status(200).send(JSON.parse(str))
            });
        })

        requisicao.end()
    },

    async getTokenGoogle(req, res) {
        const requisicao = https.request({
            host: 'sheetdb.io',
            method: 'GET',
            path: '/api/v1/c92ol4uip5e1l'
        }, (resp) => {
            var str = '';

            resp.on('data', (chunk) => {
                str += chunk.toString();
            })

            resp.on('end', function () {
                return res.status(200).send(JSON.parse(str))
            });
        })

        requisicao.end()
    },

    async getJobByID(req, res) {
        const requisicao = https.request({
            host: 'api-vanhack.azurewebsites.net',
            method: 'GET',
            path: '/v1/job/' + req.params.id
        }, (resp) => {
            var str = '';

            resp.on('data', (chunk) => {
                str += chunk.toString();
            })

            resp.on('end', function () {
                return res.status(200).send(JSON.parse(str))
            });
        })

        requisicao.end()
    },

    async getJobPipeline(req, res) {
        const requisicao = https.request({
            host: 'api-vanhack.azurewebsites.net',
            method: 'GET',
            path: '/v1/User/Jobpipeline',
            headers: {
                'Authorization': 'Bearer ' + req.headers.token
            }
        }, (resp) => {
            var str = '';

            resp.on('data', (chunk) => {
                str += chunk.toString();
            })

            resp.on('end', function () {
                return res.status(200).send(JSON.parse(str))
            });
        })

        requisicao.end()
    },

    async getEvents(req, res) {
        const requisicao = https.request({
            host: 'api-vanhack.azurewebsites.net',
            method: 'GET',
            path: '/v1/events'
        }, (resp) => {
            var str = '';

            resp.on('data', (chunk) => {
                str += chunk.toString();
            })

            resp.on('end', function () {
                return res.status(200).send(JSON.parse(str))
            });
        })

        requisicao.end()
    },

    async getJobPipelineFull(req, res) {
        const requisicao = https.request({
            host: 'api-vanhack.azurewebsites.net',
            method: 'GET',
            path: '/v1/job/search/full/'
        }, (resp) => {
            var str = '';

            resp.on('data', (chunk) => {
                str += chunk.toString();
            })

            resp.on('end', function () {
                return res.status(200).send(JSON.parse(str))
            });
        })

        requisicao.end()
    },

    async loginVanHack(req, res) {
        const curl = new Curl();

        const params = {
            'email': req.body.login,
            'password': req.body.password,
            'ReturnUrl': '',
            'RememberMe': false,
        }

        curl.setOpt(Curl.option.URL, 'https://app.vanhack.com/Account/SignIn')
        curl.setOpt(Curl.option.POSTFIELDS, querystring.stringify(params))
        curl.setOpt(Curl.option.FOLLOWLOCATION, true)
        curl.setOpt(Curl.option.VERBOSE, false)

        curl.on('end', function (statusCode, data, headers) {

            this.close();

            if (data.indexOf("Invalid user name or password") > -1) {
                return res.status(400).send()
            }

            followHttps.request({
                host: 'app.vanhack.com',
                method: 'GET',
                path: '/New'
            }, (resp) => {
                var str = '';

                resp.on('data', (chunk) => {
                    str += chunk.toString();
                })

                resp.on('end', function () {
                    return res.status(200).send(str)
                });
            }).end()

        });

        curl.on('error', curl.close.bind(curl));
        curl.perform();
    },

    async getAuthorization(req, res) {
        var hrAtual = (new Date).getTime().toString();

        var t = { "event": "mp_page_view", "properties": { "$os": "Linux", "$browser": "Chrome", "$referrer": "https://app.vanhack.com/", "$referring_domain": "app.vanhack.com", "$current_url": "https://app.vanhack.com/New/#/jobs", "$browser_version": 76, "$screen_height": 1080, "$screen_width": 1920, "mp_lib": "web", "$lib_version": "2.29.1", "time": hrAtual, "distinct_id": "16ce38e721ed03-0cf10026284af6-30760d58-1fa400-16ce38e721fcf2", "$device_id": "16ce38e721ed03-0cf10026284af6-30760d58-1fa400-16ce38e721fcf2", "$initial_referrer": "https://app.vanhack.com/Account/SignIn", "$initial_referring_domain": "app.vanhack.com", "mp_page": "https://app.vanhack.com/New/#/jobs", "mp_referrer": "https://app.vanhack.com/", "mp_browser": "Chrome", "mp_platform": "Linux", "token": mixpanel } }
        var novoToken = geradorToken(JSON.stringify(t));

        return res.json({ novoToken, btoa: Buffer.from(novoToken).toString('base64'), atob: JSON.parse(Buffer.from(novoToken, 'base64').toString()) })
    }
}

var w = null

const ke = function (a) {
    var a = (a + "").replace(/\r\n/g, "\n").replace(/\r/g, "\n"), b = "", d, c, g = 0, j;

    a = a.replace(/":"/g, '": "')
    a = a.replace(/":{"/g, '": {"')

    var pos = a.search(/":[0-9]/g);
    while (pos != -1) {
        var rec = a.substring(0, pos)
        a = rec + '": ' + a.substring(pos + 2, a.length)
        pos = a.search(/":[0-9]/g);
    }

    d = c = 0;
    g = a.length;
    for (j = 0; j < g; j++) {
        var m = a.charCodeAt(j)
            , i = w;
        128 > m ? c++ : i = 127 < m && 2048 > m ? String.fromCharCode(m >> 6 | 192, m & 63 | 128) : String.fromCharCode(m >> 12 | 224, m >> 6 & 63 | 128, m & 63 | 128);
        i !== w && (c > d && (b += a.substring(d, c)),
            b += i,
            d = c = j + 1)
    }
    c > d && (b += a.substring(d, a.length));

    return b
}

const geradorToken = (a) => {
    var b, d, e, g, j = 0, m = 0, i = "", i = [];

    if (!a) {
        return a;
    }

    a = ke(a);

    do
        b = a.charCodeAt(j++),
            d = a.charCodeAt(j++),
            e = a.charCodeAt(j++),
            g = b << 16 | d << 8 | e,
            b = g >> 18 & 63,
            d = g >> 12 & 63,
            e = g >> 6 & 63,
            g &= 63,
            i[m++] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(b) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(d) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(e) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(g);
    while (j < a.length); i = i.join("");

    switch (a.length % 3) {
        case 1:
            i = i.slice(0, -2) + "==";
            break;
        case 2:
            i = i.slice(0, -1) + "="
    }

    return i
}