const express = require('express')
const routes = express.Router()
const VanController = require('./controllers/VanController')

routes.get('/', (req, res) => {
    return res.send('Backend rodando na porta 3535')
})

routes.get('/vanhack/auth/getTokenGoogle', VanController.getTokenGoogle)
routes.get('/vanhack/auth/getCurrentAuthenticatedUser', VanController.getCurrentAuthenticatedUser)
routes.get('/vanhack/auth/getJobPipeline', VanController.getJobPipeline)

routes.get('/vanhack/getAuthorization', VanController.getAuthorization)
routes.get('/vanhack/getJobPipelineFull', VanController.getJobPipelineFull)
routes.get('/vanhack/getEvents', VanController.getEvents)
routes.get('/vanhack/getJobByID/:id', VanController.getJobByID)

routes.post('/vanhack/login', VanController.loginVanHack)

module.exports = routes