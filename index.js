const express = require('express')
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');

const app = express()
const server = require('http').Server(app);

app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api', require('./src/routes'))

server.listen(process.env.PORT || 3333);